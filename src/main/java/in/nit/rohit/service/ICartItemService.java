package in.nit.rohit.service;

import in.nit.rohit.entity.CartItem;
import java.lang.Long;
import java.util.List;

/**
 * @author:RAGHU SIR 
 *  Generated F/w:SHWR-Framework 
 */
public interface ICartItemService {
	/*
	Long saveCartItem(CartItem cartitem);

	void updateCartItem(CartItem cartitem);

	void deleteCartItem(Long id);

	CartItem getOneCartItem(Long id);

	List<CartItem> getAllCartItems();
	*/
	
	Long addCartItem(CartItem cartItem);
	void removeCartitem(Long cartItemId);
	
	List<CartItem> viewAllItems(Long custId);
	CartItem getOneCartItem(Long custId,Long prodId);
	void updateQty(Long cartItemId,Integer qty);
	int getCartItemsCount(Long custId);
	void deleteAllCartItems(List<CartItem> cartItems);

	
}
