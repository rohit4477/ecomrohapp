package in.nit.rohit.service;

import java.util.List;
import java.util.Map;

import in.nit.rohit.entity.Product;

/**
 * @author:RAGHU SIR Generated F/w:SHWR-Framework
 */
public interface IProductService {
	Long saveProduct(Product product);

	void updateProduct(Product product);

	void deleteProduct(Long id);

	Product getOneProduct(Long id);

	List<Product> getAllProducts();

	Map<Long, String> getProductIdAndName();

	List<Object[]> getProductsByBrands(Long brandId);

	List<Object[]> getProductsByCategory(Long catId);

	List<Object[]> getProductByNameMatching(String input);

}
