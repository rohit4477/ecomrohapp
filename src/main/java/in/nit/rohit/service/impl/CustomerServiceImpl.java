package in.nit.rohit.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import in.nit.rohit.constants.UserRole;
import in.nit.rohit.entity.Address;
import in.nit.rohit.entity.Customer;
import in.nit.rohit.entity.User;
import in.nit.rohit.repo.CustomerRepository;
import in.nit.rohit.service.ICustomerService;
import in.nit.rohit.service.IUserService;

/**
 * @author:RAGHU SIR Generated F/w:SHWR-Framework
 */
@Service
public class CustomerServiceImpl implements ICustomerService {
	@Autowired
	private CustomerRepository repo;

	@Autowired
	private IUserService userService;

	@Override
	@Transactional
	public Long saveCustomer(Customer customer) {
		Long id = repo.save(customer).getId();
		if (id != null) {
			User user = new User();
			user.setDisplayName(customer.getName());
			user.setEmail(customer.getEmail());
			user.setContact(customer.getMobile());
			user.setRole(UserRole.CUSTOMER);
			user.setAddress(customer.getAddress().get(0).toString());
			userService.saveUser(user);
		}
		return id;

	}

	@Override
	@Transactional
	public void updateCustomer(Customer customer) {
		repo.save(customer);
	}

	@Override
	@Transactional
	public void deleteCustomer(Long id) {
		repo.deleteById(id);
	}

	@Override
	@Transactional(readOnly = true)
	public Customer getOneCustomer(Long id) {
		return repo.findById(id).get();
	}

	@Override
	@Transactional(readOnly = true)
	public List<Customer> getAllCustomers() {
		return repo.findAll();
	}

	@Override
	public Customer findByEmail(String email) {

		return repo.findByEmail(email).get();
	}

	@Override
	public List<Address> getCustomerAddress(Long id) {

		return repo.getCustomerAddress(id);
	}
}
