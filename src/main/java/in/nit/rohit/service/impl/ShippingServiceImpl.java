package in.nit.rohit.service.impl;

import in.nit.rohit.entity.Shipping;
import in.nit.rohit.exception.ShippingNotFoundException;
import in.nit.rohit.repo.ShippingRepository;
import in.nit.rohit.service.IShippingService;
import java.lang.Long;
import java.lang.Override;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author:RAGHU SIR Generated F/w:SHWR-Framework
 */
@Service
public class ShippingServiceImpl implements IShippingService {
	@Autowired
	private ShippingRepository repo;

	@Override
	@Transactional
	public Long saveShipping(Shipping shipping) {
		return repo.save(shipping).getId();
	}

	@Override
	@Transactional
	public void updateShipping(Shipping shipping) {
		if(shipping.getId()==null || !repo.existsById(shipping.getId()))
			throw new ShippingNotFoundException("Shipping nor exist");
		else
		repo.save(shipping);
	}

	@Override
	@Transactional
	public void deleteShipping(Long id) {
		repo.delete(getOneShipping(id));
	}

	@Override
	@Transactional(readOnly = true)
	public Shipping getOneShipping(Long id) {
		return repo.findById(id).orElseThrow(()-> new ShippingNotFoundException("shipping does not exist"));
	}

	@Override
	@Transactional(readOnly = true)
	public List<Shipping> getAllShippings() {
		return repo.findAll();
	}
}
