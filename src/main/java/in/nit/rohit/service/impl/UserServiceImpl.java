package in.nit.rohit.service.impl;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import in.nit.rohit.constants.UserStatus;
import in.nit.rohit.entity.User;
import in.nit.rohit.repo.UserRepository;
import in.nit.rohit.service.IUserService;
import in.nit.rohit.util.AppUtil;

@Service
public class UserServiceImpl implements IUserService,UserDetailsService{

	@Autowired
	private UserRepository repo;
	
	@Autowired
	private BCryptPasswordEncoder encoder;
	
	@Override
	public Long saveUser(User user) {
		//Generating password
		String pwd = AppUtil.genPwd();
		System.out.println(user.getEmail()+"-"+pwd+"=-"+user.getRole());
		//Read generated pwd and encode 
		String encPwd = encoder.encode(pwd);
		
		//set back to user object
		user.setPassword(encPwd);
		
		user.setStatus(UserStatus.INACTIVE.name());
	   //user.setPassword(AppUtil.genPwd());
		return repo.save(user).getId();
		//TODO: Sending Email
	}

	@Override
	public Optional<User> findByEmail(String email) {
		
		return repo.findByEmail(email);
	}

	@Override
	public List<User> getAllUsers() {
		
		return repo.findAll();
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		// load user by username (email)
		Optional<User> opt = findByEmail(username);
	    //!opt.isPresent() || opt.get().getStatus().equals("INACTIVE");
		if(!opt.isPresent())
		{
			throw new UsernameNotFoundException("Not exist");
		}else {
			// read user object
			User user = opt.get();
			return new org.springframework.security.core.userdetails
					.User(
							user.getEmail(),
							user.getPassword(),
							Arrays.asList(
									new SimpleGrantedAuthority(
											user.getRole().name()
											)
									)
							
						);
		}
		
	}

	@Override
	@Transactional
	public void updateUserPwd(String pwd, Long userId) {
		String encPwd = encoder.encode(pwd);
		repo.updateUserPwd(encPwd, userId);

		
	}

}
