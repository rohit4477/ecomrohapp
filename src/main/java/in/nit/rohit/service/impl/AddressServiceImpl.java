package in.nit.rohit.service.impl;

import in.nit.rohit.entity.Address;
import in.nit.rohit.repo.AddressRepository;
import in.nit.rohit.service.IAddressService;
import java.lang.Long;
import java.lang.Override;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author:RAGHU SIR Generated F/w:SHWR-Framework
 */
@Service
public class AddressServiceImpl implements IAddressService {
	@Autowired
	private AddressRepository repo;

	@Override
	@Transactional
	public Long saveAddress(Address address) {
		return repo.save(address).getId();
	}

	@Override
	@Transactional
	public void updateAddress(Address address) {
		repo.save(address);
	}

	@Override
	@Transactional
	public void deleteAddress(Long id) {
		repo.deleteById(id);
	}

	@Override
	@Transactional(readOnly = true)
	public Address getOneAddress(Long id) {
		return repo.findById(id).get();
	}

	@Override
	@Transactional(readOnly = true)
	public List<Address> getAllAddresss() {
		return repo.findAll();
	}
}
