package in.nit.rohit.service.impl;

import in.nit.rohit.entity.CartItem;
import in.nit.rohit.repo.CartItemRepository;
import in.nit.rohit.service.ICartItemService;
import java.lang.Long;
import java.lang.Override;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author:RAGHU SIR 
 *  Generated F/w:SHWR-Framework 
 */
@Service
public class CartItemServiceImpl implements ICartItemService {
  @Autowired
  private CartItemRepository repo;

  /*
  @Override
  @Transactional
  public Long saveCartItem(CartItem cartitem) {
    return repo.save(cartitem).getId();
  }

  @Override
  @Transactional
  public void updateCartItem(CartItem cartitem) {
    repo.save(cartitem);
  }

  @Override
  @Transactional
  public void deleteCartItem(Long id) {
    repo.deleteById(id);
  }

  @Override
  @Transactional(
      readOnly = true
  )
  public CartItem getOneCartItem(Long id) {
    return repo.findById(id).get();
  }

  @Override
  @Transactional(
      readOnly = true
  )
  public List<CartItem> getAllCartItems() {
    return repo.findAll();
  }
  */
  
  public Long addCartItem(CartItem cartItem) {
		return repo.save(cartItem).getId();
	}

	public void removeCartitem(Long cartItemId) {
		repo.deleteById(cartItemId);
	}

	@Override
	public List<CartItem> viewAllItems(Long custId) {
		return repo.fetchCartItemsByCustomer(custId);
	}

	@Override
	public CartItem getOneCartItem(Long custId, Long prodId) {
		Optional<CartItem> opt = repo.fetchCartItemByCustomerAndProduct(custId, prodId);
		if(opt.isPresent()) 
			return opt.get();
		else
			return null;
	}

	@Override
	@Transactional
	public void updateQty(Long cartItemId, Integer qty) {
		repo.updateCartItemQty(cartItemId, qty);
	}
	
	@Override
	public int getCartItemsCount(Long custId) {
		return repo.getCartItemsCount(custId);
	}
	
	public void deleteAllCartItems(List<CartItem> cartItems) {
		repo.deleteAll(cartItems);
	}	

  
  
}
