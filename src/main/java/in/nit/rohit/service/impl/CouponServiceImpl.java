package in.nit.rohit.service.impl;

import in.nit.rohit.entity.Coupon;
import in.nit.rohit.repo.CouponRepository;
import in.nit.rohit.service.ICouponService;
import java.lang.Long;
import java.lang.Override;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author:RAGHU SIR 
 *  Generated F/w:SHWR-Framework 
 */
@Service
public class CouponServiceImpl implements ICouponService {
  @Autowired
  private CouponRepository repo;

  @Override
  @Transactional
  public Long saveCoupon(Coupon coupon) {
    return repo.save(coupon).getId();
  }

  @Override
  @Transactional
  public void updateCoupon(Coupon coupon) {
    repo.save(coupon);
  }

  @Override
  @Transactional
  public void deleteCoupon(Long id) {
    repo.deleteById(id);
  }

  @Override
  @Transactional(
      readOnly = true
  )
  public Coupon getOneCoupon(Long id) {
    return repo.findById(id).get();
  }

  @Override
  @Transactional(
      readOnly = true
  )
  public List<Coupon> getAllCoupons() {
    return repo.findAll();
  }
}
