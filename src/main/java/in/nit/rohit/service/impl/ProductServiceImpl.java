package in.nit.rohit.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import in.nit.rohit.entity.Product;
import in.nit.rohit.repo.ProductRepository;
import in.nit.rohit.service.IProductService;
import in.nit.rohit.util.AppUtil;

/**
 * @author:RAGHU SIR Generated F/w:SHWR-Framework
 */
@Service
public class ProductServiceImpl implements IProductService {
	
	@Autowired
	private ProductRepository repo;

	@Override
	@Transactional
	public Long saveProduct(Product product) {
		return repo.save(product).getId();
	}

	@Override
	@Transactional
	public void updateProduct(Product product) {
		repo.save(product);
	}

	@Override
	@Transactional
	public void deleteProduct(Long id) {
		repo.deleteById(id);
	}

	@Override
	@Transactional(readOnly = true)
	public Product getOneProduct(Long id) {
		return repo.findById(id).get();
	}

	@Override
	@Transactional(readOnly = true)
	public List<Product> getAllProducts() {
		return repo.findAll();
	}

	@Override
	public Map<Long, String> getProductIdAndName() {
		List<Object[]> list = repo.getProductIdAndName();
		return AppUtil.convertListToMapLong(list);
	}

	@Override
	public List<Object[]> getProductsByBrands(Long brandId) {
		return repo.getProductsByBrands(brandId);
	}

	@Override
	public List<Object[]> getProductsByCategory(Long catId) {
		return repo.getProductsByCategory(catId);
	}

	@Override
	public List<Object[]> getProductByNameMatching(String input) {
		return repo.getProductByNameMatching(input);
	}

}
