package in.nit.rohit.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.nit.rohit.entity.TrackOrder;
import in.nit.rohit.repo.TrackOrderRepository;
import in.nit.rohit.service.ITrackOrderService;

@Service 
public class TrackOrderServiceImpl implements ITrackOrderService {

	@Autowired
	private TrackOrderRepository repo;
	
	@Override
	public void addTrackOrder(TrackOrder to) {
		
		repo.save(to);
	}

	@Override
	public List<TrackOrder> getAllTrackOrdersByOrderid(Long orderId) {
		
		return repo.findAllTrackOrderByOrderId(orderId);
	}

}
