package in.nit.rohit.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.nit.rohit.entity.Stock;
import in.nit.rohit.repo.StockRepository;
import in.nit.rohit.service.IStockService;

@Service
public class IStockServiceImpl implements IStockService {

	@Autowired
	private StockRepository repo;
	
	@Override
	public Long saveStock(Stock stock) {
		
		return repo.save(stock).getId();
	}

	@Override
	@Transactional
	public void updateStock(Long id, Long count) {
		
		repo.updateStock(id, count);
	} 

	@Override
	public List<Stock> getStockDetails() {
		
		return repo.findAll();
	}
   
	@Override
	public Long getStockIdByProduct(Long productId) {
		
		return repo.getStockIdByProduct(productId);
	}

	

}
