package in.nit.rohit.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.nit.rohit.entity.ProductRating;
import in.nit.rohit.repo.ProductRatingRepository;
import in.nit.rohit.service.IProductRatingService;

@Service
public class ProductRatingServiceImpl implements IProductRatingService{

	@Autowired
	private ProductRatingRepository repo;
	
	@Override
	public Long createProductRating(ProductRating pr) {
	
		return repo.save(pr).getId();
	}

	@Override
	public boolean isCustomerRatedProduct(Long custId, Long prodId) {
		
		return repo.getCustomerRating(custId, prodId)>0;
	}

	@Override
	public List<ProductRating> getAllProductRatings(Long prodId) {
		
		return repo.getAllProductRatings(prodId);
	}
	
	

}
