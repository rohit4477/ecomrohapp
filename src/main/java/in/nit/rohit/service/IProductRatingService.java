package in.nit.rohit.service;

import java.util.List;

import in.nit.rohit.entity.ProductRating;

public interface IProductRatingService {
	
	Long createProductRating(ProductRating pr);
	boolean isCustomerRatedProduct(Long custId,Long prodId);
	List<ProductRating> getAllProductRatings(Long prodId);


}
