package in.nit.rohit.service;

import in.nit.rohit.entity.Shipping;
import java.lang.Long;
import java.util.List;

/**
 * @author:RAGHU SIR Generated F/w:SHWR-Framework
 */
public interface IShippingService {
	Long saveShipping(Shipping shipping);

	void updateShipping(Shipping shipping);

	void deleteShipping(Long id);

	Shipping getOneShipping(Long id);

	List<Shipping> getAllShippings();
}
