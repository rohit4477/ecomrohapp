package in.nit.rohit.service;

import java.util.List;

import in.nit.rohit.entity.Stock;

public interface IStockService {
	
	Long saveStock(Stock stock);
	void updateStock(Long id, Long Count);
	Long getStockIdByProduct(Long productId);
	List<Stock> getStockDetails();
	
}
