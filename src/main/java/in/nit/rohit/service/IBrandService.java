package in.nit.rohit.service;

import java.util.List;
import java.util.Map;

import in.nit.rohit.entity.Brand;

/**
 * @author:RAGHU SIR Generated F/w:SHWR-Framework
 */
public interface IBrandService {
	Long saveBrand(Brand brand);

	void updateBrand(Brand brand);

	void deleteBrand(Long id);

	Brand getOneBrand(Long id);

	List<Brand> getAllBrands();
	
	Map<Long,String> getBrandIdAndName();
	List<Object[]> getBrandIdAndImage();
	long totalBrands();

}
