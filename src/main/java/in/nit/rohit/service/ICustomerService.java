package in.nit.rohit.service;

import java.util.List;

import in.nit.rohit.entity.Address;
import in.nit.rohit.entity.Customer;

/**
 * @author:RAGHU SIR Generated F/w:SHWR-Framework
 */
public interface ICustomerService {
	Long saveCustomer(Customer customer);

	List<Customer> getAllCustomers();

	void updateCustomer(Customer customer);

	void deleteCustomer(Long id);

	Customer getOneCustomer(Long id);

	Customer findByEmail(String email);

	List<Address> getCustomerAddress(Long id);

}
