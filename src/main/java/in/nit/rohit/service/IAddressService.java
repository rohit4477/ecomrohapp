package in.nit.rohit.service;

import in.nit.rohit.entity.Address;
import java.lang.Long;
import java.util.List;

/**
 * @author:RAGHU SIR Generated F/w:SHWR-Framework
 */
public interface IAddressService {
	Long saveAddress(Address address);

	void updateAddress(Address address);

	void deleteAddress(Long id);

	Address getOneAddress(Long id);

	List<Address> getAllAddresss();
}
