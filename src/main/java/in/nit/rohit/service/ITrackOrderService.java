package in.nit.rohit.service;

import java.util.List;

import in.nit.rohit.entity.TrackOrder;

public interface ITrackOrderService {
	
	void addTrackOrder(TrackOrder to);
	List<TrackOrder> getAllTrackOrdersByOrderid(Long orderId);


}
