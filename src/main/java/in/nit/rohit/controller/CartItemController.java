package in.nit.rohit.controller;

import java.security.Principal;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import in.nit.rohit.entity.CartItem;
import in.nit.rohit.entity.Customer;
import in.nit.rohit.service.ICartItemService;
import in.nit.rohit.service.IProductService;
import in.nit.rohit.util.UserUtil;

/**
 * @author:RAGHU SIR 
 *  Generated F/w:SHWR-Framework 
 */
@Controller
@RequestMapping("/cartitem")
public class CartItemController {
	@Autowired
	private ICartItemService service;
	
	@Autowired
	private IProductService productService;
	
	@Autowired
	private UserUtil util;

	
    /*
	@GetMapping("/register")
	public String registerCartItem(Model model) {
		model.addAttribute("cartitem",new CartItem());
		return "CartItemRegister";
	}

	@PostMapping("/save")
	public String saveCartItem(@ModelAttribute CartItem cartitem, Model model) {
		java.lang.Long id=service.saveCartItem(cartitem);
		model.addAttribute("message","CartItem created with Id:"+id);
		model.addAttribute("cartitem",new CartItem()) ;
		return "CartItemRegister";
	}

	@GetMapping("/all")
	public String getAllCartItems(Model model,
			@RequestParam(value = "message", required = false) String message) {
		List<CartItem> list=service.getAllCartItems();
		model.addAttribute("list",list);
		model.addAttribute("message",message);
		return "CartItemData";
	}

	@GetMapping("/delete")
	public String deleteCartItem(@RequestParam Long id, RedirectAttributes attributes) {
		try {
			service.deleteCartItem(id);
			attributes.addAttribute("message","CartItem deleted with Id:"+id);
		} catch(CartItemNotFoundException e) {
			e.printStackTrace() ;
			attributes.addAttribute("message",e.getMessage());
		}
		return "redirect:all";
	}

	@GetMapping("/edit")
	public String editCartItem(@RequestParam Long id, Model model, RedirectAttributes attributes) {
		String page=null;
		try {
			CartItem ob=service.getOneCartItem(id);
			model.addAttribute("cartitem",ob);
			page="CartItemEdit";
		} catch(CartItemNotFoundException e) {
			e.printStackTrace() ;
			attributes.addAttribute("message",e.getMessage());
			page="redirect:all";
		}
		return page;
	}

	@PostMapping("/update")
	public String updateCartItem(@ModelAttribute CartItem cartitem, RedirectAttributes attributes) {
		service.updateCartItem(cartitem);
		attributes.addAttribute("message","CartItem updated");
		return "redirect:all";
	}
	
	*/
	
	@GetMapping("/add")
	public String addItemToCart(
			Principal p,
			HttpSession session,
			@RequestParam Long prodId,
			RedirectAttributes attributes
			) 
	{
		if(util.isUserLoggedIn(p)) {
			Customer cust =(Customer) session.getAttribute("customer");
			//fist check is product added to cart already or not
			CartItem cartItem = service.getOneCartItem(cust.getId(), prodId);
			if(cartItem==null) {
				//add
				CartItem newCartItem = new CartItem();
				newCartItem.setCustomer(cust);
				newCartItem.setProduct(productService.getOneProduct(prodId));
				newCartItem.setQty(1);
				service.addCartItem(newCartItem);
				attributes.addAttribute("message","Product Added to cart!");
				Integer count =(Integer)session.getAttribute("cartItemsCount");
				count = count + 1;
				session.setAttribute("cartItemsCount", count);
			} else {
				attributes.addAttribute("message","Product Quantity increased!");
				service.updateQty(cartItem.getId(), 1);
			}
		} else {
			attributes.addAttribute("message","User must login to user cart!");
		}
		attributes.addAttribute("prodId",prodId);
		return "redirect:/search/productViewById";
	}
	
	@GetMapping("/all")
	public String showCartItems(
			Principal p,
			HttpSession session,
			Model model)
	{
		Customer cust =(Customer) session.getAttribute("customer");
		List<CartItem> list = service.viewAllItems(cust.getId());
		model.addAttribute("list", list);
		return "CartItemsPage";
	}
	
	@GetMapping("/remove")
	public String removeCartItem(
			@RequestParam Long cartItemId,
			HttpSession session
			)
	{
		service.removeCartitem(cartItemId);
		Integer count =(Integer)session.getAttribute("cartItemsCount");
		count = count - 1;
		session.setAttribute("cartItemsCount", count);
		return "redirect:all";
	}
	
	@GetMapping("/increase")
	public String increaseCartItemQty(
			@RequestParam Long cartItemId
			)
	{
		service.updateQty(cartItemId, 1);
		return "redirect:/cart/all";
	}
	
	@GetMapping("/decrease")
	public String decreaseCartItemQty(
			@RequestParam Long cartItemId
			)
	{
		service.updateQty(cartItemId, -1);
		return "redirect:/cart/all";
	}

}
