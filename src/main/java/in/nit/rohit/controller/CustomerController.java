package in.nit.rohit.controller;

import in.nit.rohit.entity.Customer;
import in.nit.rohit.exception.CustomerNotFoundException;
import in.nit.rohit.service.ICustomerService;
import java.lang.Long;
import java.lang.String;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * @author:RAGHU SIR Generated F/w:SHWR-Framework
 */
@Controller
@RequestMapping("/customer")
public class CustomerController {
	@Autowired
	private ICustomerService service;

	// 1. show Register page
	@GetMapping("/register")
	public String showReg() {
		return "CustomerRegister";
	}

	// 2. save User
	@PostMapping("/save")
	public String saveUser(@ModelAttribute Customer appUser, Model model) {
		Long id = service.saveCustomer(appUser);
		model.addAttribute("message", "Customer '" + id + "' is created");
		return "CustomerRegister";
	}

	// 3. show data
	@GetMapping("/all")
	public String showData(Model model) {
		model.addAttribute("list", service.getAllCustomers());
		return "CustomerData";
	}

	/*
	 * @GetMapping("/register") public String registerCustomer(Model model) {
	 * model.addAttribute("customer", new Customer()); return "CustomerRegister"; }
	 * 
	 * @PostMapping("/save") public String saveCustomer(@ModelAttribute Customer
	 * customer, Model model) { java.lang.Long id = service.saveCustomer(customer);
	 * model.addAttribute("message", "Customer created with Id:" + id);
	 * model.addAttribute("customer", new Customer()); return "CustomerRegister"; }
	 * 
	 * @GetMapping("/all") public String getAllCustomers(Model
	 * model, @RequestParam(value = "message", required = false) String message) {
	 * List<Customer> list = service.getAllCustomers(); model.addAttribute("list",
	 * list); model.addAttribute("message", message); return "CustomerData"; }
	 */
	@GetMapping("/delete")
	public String deleteCustomer(@RequestParam Long id, RedirectAttributes attributes) {
		try {
			service.deleteCustomer(id);
			attributes.addAttribute("message", "Customer deleted with Id:" + id);
		} catch (CustomerNotFoundException e) {
			e.printStackTrace();
			attributes.addAttribute("message", e.getMessage());
		}
		return "redirect:all";
	}

	@GetMapping("/edit")
	public String editCustomer(@RequestParam Long id, Model model, RedirectAttributes attributes) {
		String page = null;
		try {
			Customer ob = service.getOneCustomer(id);
			model.addAttribute("customer", ob);
			page = "CustomerEdit";
		} catch (CustomerNotFoundException e) {
			e.printStackTrace();
			attributes.addAttribute("message", e.getMessage());
			page = "redirect:all";
		}
		return page;
	}

	@PostMapping("/update")
	public String updateCustomer(@ModelAttribute Customer customer, RedirectAttributes attributes) {
		service.updateCustomer(customer);
		attributes.addAttribute("message", "Customer updated");
		return "redirect:all";
	}
}
