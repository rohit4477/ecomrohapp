package in.nit.rohit.controller;

import in.nit.rohit.entity.Address;
import in.nit.rohit.exception.AddressNotFoundException;
import in.nit.rohit.service.IAddressService;
import java.lang.Long;
import java.lang.String;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * @author:RAGHU SIR 
 *  Generated F/w:SHWR-Framework 
 */
@Controller
@RequestMapping("/address")
public class AddressController {
	@Autowired
	private IAddressService service;

	@GetMapping("/register")
	public String registerAddress(Model model) {
		model.addAttribute("address",new Address());
		return "AddressRegister";
	}

	@PostMapping("/save")
	public String saveAddress(@ModelAttribute Address address, Model model) {
		java.lang.Long id=service.saveAddress(address);
		model.addAttribute("message","Address created with Id:"+id);
		model.addAttribute("address",new Address()) ;
		return "AddressRegister";
	}

	@GetMapping("/all")
	public String getAllAddresss(Model model,
			@RequestParam(value = "message", required = false) String message) {
		List<Address> list=service.getAllAddresss();
		model.addAttribute("list",list);
		model.addAttribute("message",message);
		return "AddressData";
	}

	@GetMapping("/delete")
	public String deleteAddress(@RequestParam Long id, RedirectAttributes attributes) {
		try {
			service.deleteAddress(id);
			attributes.addAttribute("message","Address deleted with Id:"+id);
		} catch(AddressNotFoundException e) {
			e.printStackTrace() ;
			attributes.addAttribute("message",e.getMessage());
		}
		return "redirect:all";
	}

	@GetMapping("/edit")
	public String editAddress(@RequestParam Long id, Model model, RedirectAttributes attributes) {
		String page=null;
		try {
			Address ob=service.getOneAddress(id);
			model.addAttribute("address",ob);
			page="AddressEdit";
		} catch(AddressNotFoundException e) {
			e.printStackTrace() ;
			attributes.addAttribute("message",e.getMessage());
			page="redirect:all";
		}
		return page;
	}

	@PostMapping("/update")
	public String updateAddress(@ModelAttribute Address address, RedirectAttributes attributes) {
		service.updateAddress(address);
		attributes.addAttribute("message","Address updated");
		return "redirect:all";
	}
}
