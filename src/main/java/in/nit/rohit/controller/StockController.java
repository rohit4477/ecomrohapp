package in.nit.rohit.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import in.nit.rohit.entity.Stock;
import in.nit.rohit.service.IProductService;
import in.nit.rohit.service.IStockService;

@Controller
@RequestMapping("/stock")
public class StockController {
	
	@Autowired
	private IStockService service;
	
	@Autowired
	private IProductService productService;

	private void commonui(Model model)
	{
		model.addAttribute("products", productService.getProductIdAndName()); 
	}
	
	@GetMapping("/register")
	public String showAddPage(Model model) {
		commonui(model);
		return "stockRegister";
	}
	
	
	  @PostMapping("/save")
	  public String createStock(@ModelAttribute Stock stock, Model model)
	  {
	     String message = null;  
	     Long productId =  stock.getProduct().getId();	
	     Long id = service.getStockIdByProduct(productId);
	     if(id!=null)
	        {
	       	  
		      service.updateStock(id, stock.getCount());
		      message = "stock Update!";
	        }else {
	          stock.setQoh(stock.getCount());
		      stock.setSold(Long.valueOf(0L));
		      service.saveStock(stock);
		      message = "stock update!";
	        }
	     model.addAttribute("message", message);
	     commonui(model);
	    return "StockRegister";
	 }
}
