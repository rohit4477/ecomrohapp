package in.nit.rohit.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import in.nit.rohit.entity.Product;
import in.nit.rohit.exception.ProductNotFoundException;
import in.nit.rohit.service.IBrandService;
import in.nit.rohit.service.ICategoryService;
import in.nit.rohit.service.IProductService;

/**
 * @author:RAGHU SIR Generated F/w:SHWR-Framework
 */
@Controller
@RequestMapping("/product")
public class ProductController {
	@Autowired
	private IProductService service;

	@Autowired
	private ICategoryService categoryService;

	@Autowired
	private IBrandService brandService;

	private void commonUi(Model model) {
		model.addAttribute("categories", categoryService.getCategoryIdAndName("ACTIVE"));
		model.addAttribute("brands", brandService.getBrandIdAndName());
	}

	// 1. show Register page
	@GetMapping("/register")
	public String registerProduct(Model model) {
		commonUi(model);
		// model.addAttribute("product",new Product());
		return "ProductRegister";
	}

	// 2. save product
	@PostMapping("/save")
	public String saveProduct(@ModelAttribute Product product, Model model) {
		Long id = service.saveProduct(product);
		String message = "Product '" + id + "' created!";
		model.addAttribute("message", message);
		commonUi(model);
		// model.addAttribute("product",new Product()) ;
		return "ProductRegister";
	}

	// 3. List all the product
	@GetMapping("/all")
	public String getAllProducts(Model model) {

		// @RequestParam(value = "message", required = false) String message
		List<Product> list = service.getAllProducts();
		model.addAttribute("list", list);
		// model.addAttribute("message",message);
		return "ProductData";
	}

	@GetMapping("/delete")
	public String deleteProduct(@RequestParam Long id, RedirectAttributes attributes) {
		try {
			service.deleteProduct(id);
			attributes.addAttribute("message", "Product deleted with Id:" + id);
		} catch (ProductNotFoundException e) {
			e.printStackTrace();
			attributes.addAttribute("message", e.getMessage());
		}
		return "redirect:all";
	}

	@GetMapping("/edit")
	public String editProduct(@RequestParam Long id, Model model, RedirectAttributes attributes) {
		String page = null;
		try {
			Product ob = service.getOneProduct(id);
			model.addAttribute("product", ob);
			page = "ProductEdit";
		} catch (ProductNotFoundException e) {
			e.printStackTrace();
			attributes.addAttribute("message", e.getMessage());
			page = "redirect:all";
		}
		return page;
	}

	@PostMapping("/update")
	public String updateProduct(@ModelAttribute Product product, RedirectAttributes attributes) {
		service.updateProduct(product);
		attributes.addAttribute("message", "Product updated");
		return "redirect:all";
	}
}
