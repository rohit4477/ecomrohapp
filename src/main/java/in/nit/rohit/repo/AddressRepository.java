package in.nit.rohit.repo;

import in.nit.rohit.entity.Address;
import java.lang.Long;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author:RAGHU SIR Generated F/w:SHWR-Framework
 */
public interface AddressRepository extends JpaRepository<Address, Long> {
}
