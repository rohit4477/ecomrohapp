package in.nit.rohit.repo;

import in.nit.rohit.entity.Coupon;
import java.lang.Long;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author:RAGHU SIR 
 *  Generated F/w:SHWR-Framework 
 */
public interface CouponRepository extends JpaRepository<Coupon, Long> {
}
