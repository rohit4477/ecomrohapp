package in.nit.rohit.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import in.nit.rohit.entity.Stock;

public interface StockRepository extends JpaRepository<Stock,Long> {
	
	//select s.id from Stock s INNER JOIN s.product as p where p.id=?
	//UPDATE Stock SET qoh=qoh+count where id=?
	
	@Query("SELECT s.id FROM Stock s INNER JOIN s.product as p WHERE p.id=:productId")
	Long getStockIdByProduct(Long productId);

	@Modifying 
	@Query("UPDATE Stock SET qoh=qoh+:count WHERE id=:id")
    void updateStock(Long id,Long count);
}
