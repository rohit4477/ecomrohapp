package in.nit.rohit.constants;

public enum OrderStatus {
	
	OPEN, PLACED, ORDERED, SHIPPING, 
	OUTFORDELIVERY, DELIVERED,
	CANCELLED, RETURNED


}
