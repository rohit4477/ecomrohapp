package in.nit.rohit.entity;

import java.lang.Long;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author:RAGHU SIR Generated F/w:SHWR-Framework
 */
@Entity
@Table(name = "brand_tab")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Brand {
	@Id
	@GeneratedValue
	@Column(name = "id")
	private Long id;
	
	@Column(name="brnd_name_col")
	private String name;
	
	@Column(name="brnd_code_col")
	private String code;
	
	@Column(name="brnd_tagline_col")
	private String tagLine;
	
	@Column(name="brnd_image_col")
	private String imageLink;
	
	@Column(name="brnd_note_col")
	private String note;

}
