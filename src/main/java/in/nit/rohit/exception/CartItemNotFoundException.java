package in.nit.rohit.exception;

import java.lang.RuntimeException;
import java.lang.String;

public class CartItemNotFoundException extends RuntimeException {
  public CartItemNotFoundException() {
  }

  public CartItemNotFoundException(String message) {
    super(message);
  }
}
