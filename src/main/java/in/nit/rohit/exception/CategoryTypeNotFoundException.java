package in.nit.rohit.exception;

import java.lang.RuntimeException;
import java.lang.String;

public class CategoryTypeNotFoundException extends RuntimeException {
  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

public CategoryTypeNotFoundException() {
  }

  public CategoryTypeNotFoundException(String message) {
    super(message);
  }
}
