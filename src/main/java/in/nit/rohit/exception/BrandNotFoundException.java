package in.nit.rohit.exception;

import java.lang.RuntimeException;
import java.lang.String;

public class BrandNotFoundException extends RuntimeException {
  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

public BrandNotFoundException() {
  }

  public BrandNotFoundException(String message) {
    super(message);
  }
}
