package in.nit.rohit.exception;

import java.lang.RuntimeException;
import java.lang.String;

public class AddressNotFoundException extends RuntimeException {
  public AddressNotFoundException() {
  }

  public AddressNotFoundException(String message) {
    super(message);
  }
}
