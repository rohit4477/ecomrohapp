package in.nit.rohit.exception;

import java.lang.RuntimeException;
import java.lang.String;

public class CouponNotFoundException extends RuntimeException {
  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

public CouponNotFoundException() {
  }

  public CouponNotFoundException(String message) {
    super(message);
  }
}
