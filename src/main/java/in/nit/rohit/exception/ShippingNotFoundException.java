package in.nit.rohit.exception;

import java.lang.RuntimeException;
import java.lang.String;

public class ShippingNotFoundException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ShippingNotFoundException() {
	}

	public ShippingNotFoundException(String message) {
		super(message);
	}
}
