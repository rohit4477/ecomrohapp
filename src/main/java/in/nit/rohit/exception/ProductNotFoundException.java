package in.nit.rohit.exception;

import java.lang.RuntimeException;
import java.lang.String;

public class ProductNotFoundException extends RuntimeException {
  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

public ProductNotFoundException() {
  }

  public ProductNotFoundException(String message) {
    super(message);
  }
}
