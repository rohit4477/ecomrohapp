package in.nit.rohit.exception;

import java.lang.RuntimeException;
import java.lang.String;

public class CustomerNotFoundException extends RuntimeException {
  public CustomerNotFoundException() {
  }

  public CustomerNotFoundException(String message) {
    super(message);
  }
}
